import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Product } from '../../shared/product.model';
import { Store } from '../../shared/store.model';
import { AuthService } from '../../core/services/auth.service';
import { MessageService } from '../../shared/services/message.service';
import {
  AngularFirestoreDocument,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent implements OnInit {
  productDoc: AngularFirestoreDocument<Product>;
  aProduct: Observable<any>;
  product: Product;
  stores: Store[];
  storeForm: FormGroup;
  id: string;
  closeResult = '';
  submitStatus = false;

  constructor(
    private afs: AngularFirestore,
    private modalService: NgbModal,
    private msg: MessageService,
    private fb: FormBuilder,
    public auth: AuthService
  ) {}

  ngOnInit(): void {
    this.msg.currentId.subscribe((id) => (this.id = id));
    this.productDoc = this.afs.doc<Product>(`products/${this.id}`);
    this.aProduct = this.productDoc.valueChanges();
    this.aProduct.subscribe((product) => (this.product = product));
    this.aProduct.subscribe((product) => (this.stores = product.stores));
    this.storeForm = this.fb.group({
      id: '',
      store: '',
      location: '',
      price: '',
      discount: '',
      offer: '',
      user: '',
    });
  }

  submit() {
    this.storeForm.value.id = Date.now();
    this.storeForm.value.date = Date.now().toString();
    this.storeForm.value.user = this.auth.getUserEmail();
    this.product.stores.push(this.storeForm.value);
    this.productDoc.update(this.product);
  }

  // MODAL
  open(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
