import { Component, OnInit, Input } from '@angular/core';
import { Store } from '../../../shared/store.model';
import { ProductService } from '../../../core/services/product.service';
import { Product } from '../../../shared/product.model';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Pipe, PipeTransform } from '@angular/core';
import { MessageService } from '../../../shared/services/message.service';
import { Observable } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'my-org-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
})
export class StoreComponent implements OnInit {
  productDoc: AngularFirestoreDocument<Product>;
  aProduct: Observable<any>;
  product: Product;
  id: string;

  editState = false;
  storeToEdit: Store;

  constructor(
    public auth: AuthService,
    private afs: AngularFirestore,
    private msg: MessageService
  ) {}

  ngOnInit(): void {
    this.msg.currentId.subscribe((id) => (this.id = id));
    this.productDoc = this.afs.doc<Product>(`products/${this.id}`);
    this.aProduct = this.productDoc.valueChanges();
    this.aProduct.subscribe((product) => (this.product = product));
  }

  edit(event: any, store: Store) {
    this.editState = true;
    this.storeToEdit = store;
    console.log(this.product.stores[0]);
  }

  update(store: Store) {
    this.storeToEdit.user = this.auth.getUserEmail();
    this.storeToEdit.date = Date.now().toString();
    this.productDoc.update(this.product);
    this.clear();
  }

  clear() {
    this.editState = false;
  }
}
