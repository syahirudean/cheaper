import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'my-org-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
})
export class AddProductComponent implements OnInit {
  task: AngularFireUploadTask;
  productForm: FormGroup;
  file: File;
  image = false;
  imgPath: string;

  snapshot: Observable<any>;
  downloadURL: string;

  // Form state
  loading = false;
  success = false;

  constructor(
    private afStorage: AngularFireStorage,
    private route: Router,
    private fb: FormBuilder,
    private afs: AngularFirestore,
    public auth: AuthService
  ) {}

  ngOnInit(): void {
    this.productForm = this.fb.group({
      id: '',
      brand: ['', [Validators.required]],
      model: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      imgUrl: '',
      user: '',
      stores: this.fb.array([]),
    });
  }

  get storesForms() {
    return this.productForm.get('stores') as FormArray;
  }

  addStores() {
    const store = this.fb.group({
      id: [Date.now()],
      store: [''],
      location: [''],
      price: [''],
      discount: [''],
      offer: [''],
      date: [Date.now().toString()],
    });

    this.storesForms.push(store);
  }

  deleteStore(i: any) {
    this.storesForms.removeAt(i);
  }

  // Upload file to FS storage
  upload($event: any) {
    this.file = $event.target.files[0];
    // Generate random ID
    // const randomId = Math.random().toString(36).substring(2);
    // The storage path
    const imgName =
      this.productForm.value.brand +
      '_' +
      this.productForm.value.model +
      '_' +
      this.productForm.value.amount;
    const fileName = imgName.split(' ').join('_');
    this.imgPath = `products/${fileName}`;
    // The main task (upload Image to Firestorage)
    this.task = this.afStorage.upload(this.imgPath, this.file);
    this.image = true;
  }

  async submitHandler() {
    this.loading = true;
    if (this.image === true) {
      const newImgPath = `${this.imgPath}_500x500`;
      // Reference to storage bucket
      const ref = this.afStorage.ref(newImgPath);
      // Get the URL from ref (Firestorage), added to the form and submit to FIrestore
      this.task
        .snapshotChanges()
        .pipe(
          finalize(async () => {
            this.downloadURL = await ref.getDownloadURL().toPromise();
            // Form Edit Section START
            this.productForm.value.imgUrl = this.downloadURL;
            this.productForm.value.user = this.auth.getUserEmail();
            const formValue = this.productForm.value;
            // Form edit section END
            try {
              await this.afs.collection('products').add(formValue);
              console.log(formValue);
              this.success = true;
            } catch (err) {
              console.error(err);
            }
          })
        ) // Display URL
        .subscribe();
    } else {
      alert('Please upload the picture of product');
    }
    this.loading = false;
  }
}
