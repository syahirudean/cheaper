import { Component, OnInit } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { ProductService } from '../../core/services/product.service';
import { Product } from '../../shared/product.model';
import { MessageService } from '../../shared/services/message.service';

@Component({
  selector: 'my-org-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit {
  private alertDoc: AngularFirestoreDocument<any>;
  products: Product[];
  state = true;
  inventory = false;
  info: Observable<any>;
  // search: string; // TODO: Implement search when site grows

  constructor(
    private productService: ProductService,
    private msg: MessageService,
    private afs: AngularFirestore,
    private route: Router,
    public auth: AuthService
  ) {
    this.alertDoc = afs.doc<any>('alert/Smh4UMJ7tPu2fGwigVHQ');
    this.info = this.alertDoc.valueChanges();
  }

  ngOnInit(): void {
    /*this.msg.currentSearch.subscribe(
      (search) =>
        (this.search = search
          .toLowerCase()
          .split(' ')
          .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
          .join(' '))
    );*/
    this.productService.getProducts().subscribe(
      (products) => this.checkInventory(products)
      // TODO: Implement when product starts to increase;
      /*(this.products = products.filter((product) => {
          return product.brand.toLowerCase() === this.search.toLowerCase();
        }))*/
    );
  }

  sendId(id: string) {
    this.msg.sendId(id);
    this.productService.sendId(id);
    this.route.navigate(['/page']);
  }

  checkInventory(products: any) {
    if (products.length > 0) {
      this.products = products;
      this.inventory = true;
    }
  }
}
