import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../core/services/product.service';
import { MessageService } from '../../shared/services/message.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  search = '';

  constructor(
    public productService: ProductService,
    public route: Router,
    private msg: MessageService
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    const wsRegex = /^\s+|\s+$/g;
    const afterWsRegex = this.search.replace(wsRegex, '');
    if (afterWsRegex.match(/^[0-9a-zA-Z$]+/)) {
      this.msg.search(this.search);
      console.log(this.search);
      this.route.navigate(['/results']);
    } else {
      alert('Sorry, there is nothing to search!');
    }
  }
}
