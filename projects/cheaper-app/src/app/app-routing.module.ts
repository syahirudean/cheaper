import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'results',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./features/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./features/admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: 'page',
    loadChildren: () =>
      import('./features/page/page.module').then((m) => m.PageModule),
  },
  {
    path: 'results',
    loadChildren: () =>
      import('./features/results/results.module').then((m) => m.ResultsModule),
  },
  {
    path: 'add-product',
    loadChildren: () =>
      import('./features/add-product/add-product.module').then(
        (m) => m.AddProductModule
      ),
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
