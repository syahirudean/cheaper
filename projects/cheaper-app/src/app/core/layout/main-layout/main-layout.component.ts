import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from '../../services/auth.service';
import { MessageService } from '../../../shared/services/message.service';
import { Product } from '../../../shared/product.model';
import { ProductService } from '../../services/product.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  products: Product[];
  search = '';
  closeResult = '';
  title: string;
  body: string;

  constructor(
    public route: Router,
    private modalService: NgbModal,
    private msg: MessageService,
    private productService: ProductService,
    public auth: AuthService,
    private afs: AngularFirestore
  ) {}

  ngOnInit(): void {
    this.productService
      .getProducts()
      .subscribe((products) => (this.products = products));
  }

  onSubmit() {
    const wsRegex = /^\s+|\s+$/g;
    const afterWsRegex = this.search.replace(wsRegex, '');
    if (
      afterWsRegex.match(/^[0-9a-zA-Z$]+/) &&
      this.products.find(
        (product) => product.brand.toLowerCase() === this.search.toLowerCase()
      )
    ) {
      this.msg.search(this.search);
      this.route.navigate(['/results']);
    } else {
      alert('Sorry, there is nothing to search!');
    }
  }

  // MODAL
  open(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  submit() {
    const report = {
      userEmail: this.auth.getUserEmail(),
      title: this.title,
      body: this.body,
    };

    this.afs.collection<any>('reports').add(report);
    alert('Thank you!');
  }
}
