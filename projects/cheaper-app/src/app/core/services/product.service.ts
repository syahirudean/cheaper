import { Injectable } from '@angular/core';
import { Product } from '../../shared/product.model';

import { map } from 'rxjs/operators';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { Store } from '../../shared/store.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  productsCollection: AngularFirestoreCollection<Product>;
  productDoc: AngularFirestoreDocument<Product>;
  products: Observable<Product[]>;
  product: Observable<any>;
  aProduct: Product;
  id: string;

  constructor(public afs: AngularFirestore) {
    // this.products = this.afs.collection('products').valueChanges();
    this.productDoc = this.afs.doc<Product>(`products/${this.id}`);
    this.product = this.productDoc.valueChanges();

    this.productsCollection = this.afs.collection('products', (ref) =>
      ref.orderBy('brand', 'asc')
    );
    this.products = this.productsCollection.snapshotChanges().pipe(
      map((changes) => {
        return changes.map((a) => {
          const data = a.payload.doc.data() as Product;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }

  getId() {
    return this.id;
  }

  getProducts() {
    return this.products;
  }

  getProduct() {
    return this.product;
  }

  getStores() {
    return this.aProduct.stores;
  }

  updateProduct(product: Product) {
    this.productDoc.update(product);
  }

  sendId(id: string) {
    // this.productDoc = this.afs.doc<Product>(`products/${id}`);
    this.id = id;
    // this.products.subscribe((products) => console.log(products));
  }
}
