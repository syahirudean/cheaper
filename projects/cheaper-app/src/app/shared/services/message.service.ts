import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  private idSource = new BehaviorSubject<string>('');
  currentId = this.idSource.asObservable();
  private searchSource = new BehaviorSubject<string>('');
  currentSearch = this.searchSource.asObservable();
  result = this.currentSearch.subscribe((word) => {
    return word;
  });
  constructor() {
  }

  sendId(id: string) {
    this.idSource.next(id);
  }

  search(product: string) {
    this.searchSource.next(product);
  }
}
