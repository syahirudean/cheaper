// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC7Fu32J7D07wZnijQm5OAjyKNcy7xCMQA',
    authDomain: 'pricelist-9fc86.firebaseapp.com',
    databaseURL: 'https://pricelist-9fc86.firebaseio.com',
    projectId: 'pricelist-9fc86',
    storageBucket: 'pricelist-9fc86.appspot.com',
    messagingSenderId: '849460242206',
    appId: '1:849460242206:web:8cbb6fd20c5efd14e38b0f',
    measurementId: 'G-QVML0MQ5YR',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
