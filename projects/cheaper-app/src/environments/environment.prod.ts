export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC7Fu32J7D07wZnijQm5OAjyKNcy7xCMQA',
    authDomain: 'pricelist-9fc86.firebaseapp.com',
    databaseURL: 'https://pricelist-9fc86.firebaseio.com',
    projectId: 'pricelist-9fc86',
    storageBucket: 'pricelist-9fc86.appspot.com',
    messagingSenderId: '849460242206',
    appId: '1:849460242206:web:8cbb6fd20c5efd14e38b0f',
    measurementId: 'G-QVML0MQ5YR',
  },
};
