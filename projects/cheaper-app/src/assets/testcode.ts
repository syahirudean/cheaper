async submitHandler() {
    if (this.image === true) {
      // Generate random ID
      const randomId = Math.random().toString(36).substring(2);
      // image name
      const imgName = this.productForm.value.model.split(' ').join('_');
      // The storage path
      const path = `products/${imgName}_${randomId}`;
      // Reference to storage bucket
      const ref = this.afStorage.ref(path);
      // The main task (upload Image to Firestorage)
      this.task = this.afStorage.upload(path, this.file);

      // Get the URL from ref (Firestorage), added to the form and submit to FIrestore
      this.task
        .snapshotChanges()
        .pipe(
          finalize(async () => {
            this.downloadURL = await ref.getDownloadURL().toPromise();
            /*this.afs
            .collection('products')
            .add({ downloadURL: this.downloadURL, path });*/
            this.loading = true;
            
            // Form Edit Section START
            this.productForm.value.imgUrl = this.downloadURL;
            this.productForm.value.user = this.auth.getUserEmail();
            const formValue = this.productForm.value;
            // Form edit section END
            try {
              await this.afs.collection('products').add(formValue);
              console.log(formValue);
              this.success = true;
            } catch (err) {
              console.error(err);
              alert('Oops! Look like an error. Try again leter');
            }
          })
        ) // To display URL
        .subscribe();

      this.loading = false;
    } else {
      alert('Please upload the picture of product');
    }
  }